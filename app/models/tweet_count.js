/**
  * Mongoose Models and their Schema
  **/

var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;
var TweetCountSchema   = new Schema({
    tweet_count_id: {type:String, unique:true},
    javascript: {type:Number, default:0, required:true},
    jquery: {type:Number, default:0, required:true},
    nodejs: {type:Number, default:0, required:true},
    angularjs: {type:Number, default:0, required:true},
    html5: {type:Number, default:0, required:true},
    css3: {type:Number, default:0, required:true},
    mongodb: {type:Number, default:0, required:true},
    createdAt: { type: Date, default: Date.now }
});
module.exports = mongoose.model('TweetCount', TweetCountSchema);
