/**
  * Node Server
  **/
var express = require('express');
var fs = require('fs');
var bodyParser = require('body-parser');
var mongodb = require('mongodb');
var monk = require('monk');
var Twitter = require('twitter');
var request = require('request');
var app = express();
var router = express.Router();
var port = process.env.PORT || '3000';
var config = require('./config/config.js');
var db =  monk(config.mongo_url);
var CronJob = require('cron').CronJob;
var today = new Date();
var u_id = today.toString().substring(0,15).replace(/ /g, '');

var job = new CronJob({
  cronTime: '00 01 12 * * 0-6',
  onTick: function() {
    var counts = db.get('counts');
    counts.insert({
      "unique_id":u_id,
      "year": today.getFullYear(),
      "month":today.getMonth(),
      "date":today.getDate(),
      "javascript":0,
      "nodejs":0,
      "angularjs":0,
      "jquery":0,
      "html5":0,
      "css3":0,
      "mongodb":0
    }, function(err, result){
        console.log("insert result: "+result);
    });
    counts.index('unique_id', { unique: true }); 
  },
  start: false,
});
job.start();

// set 1
var javascript = new Twitter({
  consumer_key: config.consumer_key1,
  consumer_secret: config.consumer_secret1,
  access_token_key: config.access_token_key1,
  access_token_secret: config.access_token_secret1
});
var nodejs = new Twitter({
  consumer_key: config.consumer_key1,
  consumer_secret: config.consumer_secret1,
  access_token_key: config.access_token_key1,
  access_token_secret: config.access_token_secret1
});

// set 2
var jquery = new Twitter({
  consumer_key: config.consumer_key2,
  consumer_secret: config.consumer_secret2,
  access_token_key: config.access_token_key2,
  access_token_secret: config.access_token_secret2
});
var angularjs = new Twitter({
  consumer_key: config.consumer_key2,
  consumer_secret: config.consumer_secret2,
  access_token_key: config.access_token_key2,
  access_token_secret: config.access_token_secret2
});


// set 3
var html5 = new Twitter({
  consumer_key: config.consumer_key3,
  consumer_secret: config.consumer_secret3,
  access_token_key: config.access_token_key3,
  access_token_secret: config.access_token_secret3
});
var mongodb = new Twitter({
  consumer_key: config.consumer_key3,
  consumer_secret: config.consumer_secret3,
  access_token_key: config.access_token_key3,
  access_token_secret: config.access_token_secret3
});

// var css3 = new Twitter({
//   consumer_key: config.consumer_key,
//   consumer_secret: config.consumer_secret,
//   access_token_key: config.access_token_key,
//   access_token_secret: config.access_token_secret
// });

app.use(express.static('.'));
app.use(express.static('./app/views'));
app.use(bodyParser.json()); 
app.use(bodyParser.urlencoded({'extended':'false'}));


javascript.stream('statuses/filter', {track: 'javascript'}, function(stream) {
  stream.on('data', function(tweet) {
    console.log("javascript=>",tweet.text);
    download(tweet.user.profile_image_url, tweet.user.screen_name+'.png', function(){});
    var counts = db.get('counts');
    counts.update({"unique_id":u_id},{"$inc":{"javascript":1}}, function(err, result){
      if(err) console.log(err);
    });
  });
 
  stream.on('error', function(error) {
    throw error;
  });
});

mongodb.stream('statuses/filter', {track: 'mongodb'}, function(stream) {
  stream.on('data', function(tweet) {
    console.log("mongodb=>",tweet.text);
    download(tweet.user.profile_image_url, tweet.user.screen_name+'.png', function(){});
    var counts = db.get('counts');
    counts.update({"unique_id":u_id},{"$inc":{"mongodb":1}}, function(err, result){
      if(err) console.log(err);
    });
  });
 
  stream.on('error', function(error) {
    throw error;
  });
});

nodejs.stream('statuses/filter', {track: 'nodejs'}, function(stream) {
  stream.on('data', function(tweet) {
     console.log("nodejs=>",tweet.text);
    download(tweet.user.profile_image_url, tweet.user.screen_name+'.png', function(){});
    var counts = db.get('counts');
    counts.update({"unique_id":u_id},{"$inc":{"nodejs":1}}, function(err, result){
      if(err) console.log(err);
    });
  });
 
  stream.on('error', function(error) {
    throw error;
  });
});


jquery.stream('statuses/filter', {track: 'jquery'}, function(stream) {
  stream.on('data', function(tweet) {
    console.log("jquery=>",tweet.text);
    download(tweet.user.profile_image_url, tweet.user.screen_name+'.png', function(){});
    var counts = db.get('counts');
    counts.update({"unique_id":u_id},{"$inc":{"jquery":1}}, function(err, result){
      if(err) console.log(err);
    });
  });
 
  stream.on('error', function(error) {
    throw error;
  });
});

html5.stream('statuses/filter', {track: 'html5'}, function(stream) {
  stream.on('data', function(tweet) {
    console.log("html5=>",tweet.text);
    download(tweet.user.profile_image_url, tweet.user.screen_name+'.png', function(){});
    var counts = db.get('counts');
    counts.update({"unique_id":u_id},{"$inc":{"html5":1}}, function(err, result){
      if(err) console.log(err);
    });
  });
 
  stream.on('error', function(error) {
    throw error;
  });
});

// css3.stream('statuses/filter', {track: 'css3'}, function(stream) {
//   stream.on('data', function(tweet) {
//     console.log("css3=>",tweet.text);
//     download(tweet.user.profile_image_url, tweet.user.screen_name+'.png', function(){});
//     var counts = db.get('counts');
//     counts.update({"unique_id":u_id},{"$inc":{"css3":1}}, function(err, result){
//       if(err) console.log(err);
//     });
//   });
 
//   stream.on('error', function(error) {
//     throw error;
//   });
// });


var download = function(uri, filename, callback){
    request.head(uri, function(err, res, body){
    var r = request(uri).pipe(fs.createWriteStream(__dirname+'/TwitterImages/'+filename));
    r.on('close', callback);
  });
};

// create_record();
// function create_record(){
//   console.log("create_record()")
//   var counts = db.get('counts');
//   counts.insert({
//     "unique_id":u_id,
//     "year": today.getFullYear(),
//     "month":today.getMonth()+1,
//     "date":today.getDate(),
//     "javascript":0,
//     "nodejs":0,
//     "angularjs":0,
//     "jquery":0,
//     "html5":0,
//     "css3":0,
//     "mongodb":0
//   }, function(err, result){
//       if(err) console.log(err);
//   });
//   counts.index('unique_id', { unique: true }); 
// }

app.get('/',function(req,res){
    res.redirect('main.html');
});


app.listen(port, function(){
	console.log("Application is running on "+port);
});