
/**
  * Configure yours global variables and urls here
  **/

var fs = require('fs');
var Secure_Keys = JSON.parse(fs.readFileSync('private.json'));
module.exports = {
	mongo_url : Secure_Keys.mongolab_url,

    consumer_key: Secure_Keys.twitter.Consumer_Key,
    consumer_secret: Secure_Keys.twitter.Consumer_Secret,
    access_token_key: Secure_Keys.twitter.Access_Token,
    access_token_secret: Secure_Keys.twitter.Access_Token_Secret //,

	// consumer_key1: Secure_Keys.twitter1.consumer_key,
 //  	consumer_secret1: Secure_Keys.twitter1.consumer_secret,
 //  	access_token_key1: Secure_Keys.twitter1.access_token_key,
 //  	access_token_secret1: Secure_Keys.twitter1.access_token_secret,


	// consumer_key2: Secure_Keys.twitter2.consumer_key,
 //  	consumer_secret2: Secure_Keys.twitter2.consumer_secret,
 //  	access_token_key2: Secure_Keys.twitter2.access_token_key,
 //  	access_token_secret2: Secure_Keys.twitter2.access_token_secret,


	// consumer_key3: Secure_Keys.twitter3.consumer_key,
 //  	consumer_secret3: Secure_Keys.twitter3.consumer_secret,
 //  	access_token_key3: Secure_Keys.twitter3.access_token_key,
 //  	access_token_secret3: Secure_Keys.twitter3.access_token_secret
}